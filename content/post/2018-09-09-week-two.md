---
title: Week Two
date: 2018-09-09
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 


## What did you do this past week?
Last week we learned about assertions, testing, and coverage. We learned that asserts are better for preconditions and postconditions, but not necessarily testing because of the fact that the program terminates the first time a test fails. I also started looking at the first project and planned out most of my psudo code with modifications to make it more efficient, but I haven't actually implemented any of it yet. It has also been interesting learning about extreme programming and how it differs from past software development life cycles' I've worked within.

## What's in your way?
I have multiple on-site interviews coming up so will need to schedule them and decide which classes I will have to miss. I have three classes tuesday and thursday with SWE my only class on monday, wednesday, and friday. While I'll probably end up mising this class for most of my interviews, im worried about possibly getting behind. I also need to consider that if I miss too many classes then my grade will be reduced by a letter. I also heard on Piazza that there were a few lint errors with the first project which may be something that will be in my way when I start it. I also need to decide what kind of development environment I want to use for the project and whether ot not gitlab's built in IDE will be sufficient enough.

## What will you do next week?
Next week I'll be working on then finishing the first project. I'll also read chapters nine through fifteen of the textbook and get more familiar with using gitlab.


## What's your experience of the class?
So far I'm really enjoying the class. I'm learning a lot and have been able to make a lot of connections between the material taught and my projects at previous internships. I also really love the in class participation and like how it's more of a conversation rather than the student being called on just answering a single question.


## What's your pick-of-the-week or tip-of-the-week?
My tip-of-the-week is that if you accidentally close a browser tab, you can bring it back by typing CTRL+Shift+T. I didn't know this until recently and it has been very helpful if I accidentally closed a tab or wanted to bring it back ten minutes later.
