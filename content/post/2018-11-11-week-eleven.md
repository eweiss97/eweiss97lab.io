---
title: Week Eleven
date: 2018-11-11
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 


## What did you do this past week?
This past week in class we learned about joins and spent time with our customers and developers providing and recieving feedback. Outside of class my group started on the next phase of the project. I specifically helped change the backend to support filtering and searching, then spent time creating the frontend search page for every model. Aside from that I updated our postman documentation and tests for our refined API.

## What's in your way?
We have a few annoying bugs within react where our states aren't updating or resetting properly. For example, we have a button that clears filters but we can't figure out why it requires two clicks to send an updated API call to the backend. We have a similar bug where if we enter a new query for searching then the current page number is kept instead of being reset to 1 even though the data shown correctly coresponds to the first page.

## What will you do next week?
Next week we'll finish project 4 and continue learning about SQL. I'll also be missing class on Friday since I'm flying to San Francisco for an onsite interview. I'm starting to get close to the 10 absence mark so will need to be careful not to miss to many more classes. I also have a project due for another class Tuesday morning that I haven't started on so will likely be spending a lot of time tomorrow after class working on it.

## What was your experience in learning SQL?
I thought that learning SQL was interesting, especially since we first got into the logic of relational algebra before we touched the syntax. Joins were a bit confusing at first but I think I'm starting to understand how they work better. Before this class I had only ever used basic SQL commands so it's nice to have a formal structure to learn it.

## What's your pick-of-the-week or tip-of-the-week?
My tip of the week is to really make sure both the front end and backend developers are on the same page before writing code. If you're not on the same page then both subgroups might end up writing code that isnt too compatible and will lead to a lot of wasted time.




