---
title: Week Twelve
date: 2018-11-18
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 

## What did you do this past week?
We started off the week by finishing project 4 and learning about altering tables in SQL. On Wednesday and Friday, we started to learn about refactoring in Java and had a few examples within hackerrank. I had to miss class Friday for an interview in the Bay Area, and am glad to have finally accepted an internship offer for next summer and to be done interviewing! I'm at 8 absenses now so will have to try my best not to miss any more classes.

## What's in your way?
The biggest thing in our way is the break thats coming up, since our group technically only has 4 days left to finish project 5. Quite a few of us live in Dallas so will have to double check the project policy about how many members need to be present to work together and see if we could work during the break.

## What will you do next week?
On Monday we'll continue learning about refactoring with strategy patterns. After that is thanksgiving and the project 5 deadline. Since the presentation and visualizations aren't actually due until we present we'll mainly be focusing on refactoring and documentation. There are also a few bugs with our current project 4 code such as search not updating correctly if the user searches twice from the search page which we'll have to look into.

## What was your experience of Project 4?
Project 4 seemed pretty short compared to the previous projects. The most time consuming part was probably updating our postman documentation after adding a bunch of new API calls for the frontend to use for filtering and searching. Overall, implementing this phase was pretty straightforward. We did run into one issue though where one of our unit tests was testing a filter that filtered by data within the past month, and it took us a while to realize the reason one of our tests was failing because of it.

## What's your pick-of-the-week or tip-of-the-week?
My pick of the week would have to be react-highlight which is a component for highlighting matches in a phrase. This made it really easy to satisfy the search requirement for project 4. 





