---
title: Week Eight
date: 2018-10-21
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 


## What did you do this past week?
Last week we took our first test, spent time in class providing feedback to other group's about their projects, and learned about the basics of SQL in python. Outside of class my group has been working on project 2. I specifically have been helping set up our database and have been working on the backend using flask. One of the biggest problems I've been having is that it seems like a lot of the documentation for Flask is spread out and there aren't many good examples available for what my group wants to do. 

## What's in your way?
The biggest thing in our way right now is figuring out how to switch from S3 to EC2 instances on AWS in order to host our dynamic pages. We'll need to figure out how to have a program running on localhost accessible publicly then figure out how to connect it to our domain and subdomains respectively. We'll also need to figure out how to add the api subdomain to the backend while doing this. Another problem is that we've blown through our free credit this month from having two static buckets running 24/7, so we'll need to think of a way to reduce the server costs for the rest of the semester.

## What will you do next week?
Next week we'll be finishing up project 2. For the most part the backend is done so we'll need to focus on the front end. The logic shouldn't be too complex but a lot of the work will be learning how to use react.

## What was your experience of Test 1?
Overall, I thought that the first exam was very straightforward, especially if you listened in class. The problem that I have the most trouble with was the zip problem due to the syntax, but I eventually got it. What I really liked about the exam was that it was through hackerrank and simulated the job as a software engineer where you're able to run and edit your code compared to previous classes where you were expected to write bug-free code the first time.

## What's your pick-of-the-week or tip-of-the-week?
My tip of the week is to make backups of your databases. Nothing is worse than having a working database and accidentally adding duplicate rows, then having to spending time trying to figure out how to remove them while reverting an auto incrementing primary key.



