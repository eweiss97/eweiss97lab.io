---
title: Final Entry
date: 2018-12-11
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 

## What did you like the least about the class?
Overall I really enjoyed the class, but if I have to put something here I guess it would be that the technical reports were too open ended. My group often lost points for things not specified on the rubric or for something we omitted because we felt it would be too redundant.

## What did you like the most about the class?
I really liked how the projects were structured and split up. Within the group projects specifically, I thought it was a nice pace to first start with a static site, make it dynamic, then later once you have a stable frontend and backend add additional functionality to it.

## What's the most significant thing you learned?
The biggest thing I learned was how to work better and more efficiently in a team. This was the first time I worked in a large group (6 people) on a single project and learned a lot about time management and communication.

## How many hours a week did you spend coding/debugging/testing for this class?
It usually varied week by week, but on average I would say about 8-10 hours per week for some of the lighter projects and more towards 25-30 hours for the heavier ones, but that's mainly because our group usually waited a week or two after the projects were assigned to start on them.

## How many hours a week did you spend reading/studying for this class?
Probably about 45 minutes a week just to quickly review the previous lecture and read the weekly readings. For each exam I probably spent an additional 5-6 hours reviewing for each one.

## How many lines of code do you think you wrote?
Throughout every project combined, probably about 2000. If I only count code that's currently deployed on our website then maybe closer to 1000.

## What required tool did you not know and now find very useful?
This was my first time working with AWS and I definitely learned a lot about how to actually deploy and scale an application. For previous projects or internships I've only ever written code on localhost so it was cool seeing how to actually make it available online and redeploy updated versions.

## What's the most useful Web dev tool that your group used that was not required?
Slack, which made communication for our project really easy. We had separate channels for each part of development (frontend, backend, testing, server configuration, etc) which helped us stay organized and keep track of what everyone was working on.

## If you could change one thing about the course, what would it be?
I would get rid of the "civil" requirement for each project. I understand the point of it, but since theres only so many possible causes that would work for the projects, I think it would be fun to see what each group could come up with, especially when they're more passionate about their theme.

Overall, I would highly recommend this course and can definitely say that I've learnt the most here than I have any other course I've taken at UT to this date.