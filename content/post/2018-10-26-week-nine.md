---
title: Week Nine
date: 2018-10-26
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 


## What did you do this past week?
This past week I spent way too much time figuring out how to deploy our frontend to AWS. We were running into an issue where we were getting a cors header error whenever we tried calling the backend and it took a lot of researching to figure out that we could add a header to our backend to fix the issue. I also worked on the front end a bit and implemented one of the instance and modal pages. In class, we learned about theta join, natural join, and cross join which are a part of relational algebra.

## What's in your way?
The only thing that's in my way is how busy I'm going to be throughout the next few days. I'm flying to Seattle for an onsite interview, then am flying back at midnight with an exam for another class and phone interview the next day. I tried to put in a lot of work this week but it'll be up to the rest of my group to finish the project since I'll be out of town. Luckily though all we have left to do is a bit of testing and adding a bit more info to the technical report.

## What will you do next week?
Next week after finishing our project we'll be continuing to learn about SQL in class and likely get started on project 3. Specifically, we'll probably start by modifying the model pages to support filtering and sorting by our attributes. Within class we'll also be listening to Dr. Rich and Dr. Cline for a second time. 

## What did you think of the talk by Dr. Rich and Dr. Cline?
I found it really interesting; I never knew to what extent ethics were engrained in computer science before listening to their talk. One thing I found interesting in particular is how it's really hard to define an ethical line when it comes to data.

## What's your pick-of-the-week or tip-of-the-week?
My pick of the week is cors.io. It's a proxy that makes it really easy for a frontend to fetch data on a site that doesn't have an access control header. One way it can be used is to grab a raw file from a public gitlab repo to parse for data such as number of unit tests.



