---
title: Week Three
date: 2018-09-16
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 


## What did you do this past week?
This week in class we learned about exceptions and types in Python. We particulary learned about the cases in which using exceptions would be better than returning a reference or updating a global variable and why the prior is better for error handling. We also learned that exceptions are better for user-errors than assertions because the program shouldn't halt when a user gives incorrect input, but should instead tell the user how to correct their input.

## What's in your way?
With interview season coming up, I realized how behind I am in preparing for interviews. After taking Algorithms last semester I was feeling great but forgot a lot of material - especially dynamic programming - because I haven't practiced all summer. This is definitely going to take up a lot of time that could have been used elsewhere.

## What will you do next week?
I think that next week we'll be introduced to Project 2 which is the classes' first group project. My group and I will probably spend next week setting up our servers and configuring them and deciding whether to use GCP or AWS. We'll also likely make an approach of how to solve the project and learning about the tools we'll need to complete the project. From looking at the schedule, the class will also learn about pair programming next week which is probably what we'll have to use on the group projects.

## What's your experience of the in-class exercises?
I think the exercises are engaging and are teaching us a lot about good software design. One thing that I really like is how the professor relates concepts in Python back to Java. We used Java for our intro to programming courses and it's really helpful to those who aren't as comfortable with Python.

## What's your pick-of-the-week or tip-of-the-week?
My pick of the week for interview preparating is LeetCode. It's an online platform that has a bunch of practice interview questions. Unlike other interview sites like HackerRank, all of the user input and output is abstracted and it's overall a much more user-friendly site. You can sort by programming topic, see how your solution ranks against others, and even view other's solutions if you get stuck on a problem. I don't, however, recommend the paid version since all it does is let you take mock interviews which just gives you a random problem with a timer.
