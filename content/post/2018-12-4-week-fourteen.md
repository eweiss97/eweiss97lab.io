---
title: Week Fourteen
date: 2018-12-04
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 

## What did you do this past week?
This past week we finished learning about refactoring in class. Outside of class my group worked with D3 to produce our visualizations. After turning in the project we started working on our presentation slides and the visualizations for our developers.

## What's in your way?
Currently in our way is an issue where browsers don't like the fact that our site is HTTPS and our developers site is HTTP due to mixed data. Once deployed on AWS our webapp is having trouble calling the backend for our developers. We think a proxy may help but will need to get this working by the time we present tomorrow morning.

## What will you do next week?
This week we'll present and listen to the other groups. After that we just have our final blog post and second exam. As far as our presentation goes, we have about half of our slides done and still need to meet up to practice and make sure that we dont go over 15 minutes. Lastly I'll also start studying and reviewing for our next exam.

## If you went, what did you think of the talk by Google?
I thought it was a very interesting perspective and can very much relate with not knowing exactly what I want to do after I graduate. I still haven't decided whether or not I should apply to grad school or just start working full time after graduating. The one piece of advice I really liked was the idea of applying for a PhD instead of a masters so you don't have to pay then can drop out after reaching a masters degree.

## What was your experience of the refactoring topics?
I thought it was really interesting. I never really sat down and spent time learning abstract classes and the like and definitely learned a lot of styles and approaches to refactor that lead to good code hygine.

## What's your pick-of-the-week or tip-of-the-week?
My tip of the week is to always think about resources before implementing a feature. One of our original visualizations for our customers ended up taking over 600 API Calls which took way too long to load. After thinking about it and refactoring for a bit using dictionaries, we were able to decrease this number down to 2.





