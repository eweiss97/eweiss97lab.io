---
title: Week One
date: 2018-09-02
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 


## What did you do this past week?
This past week I learned about containerization and docker. I also became familiar with the structure of the class and started thinking about whether GCP or AWS would be better for the class projects. Lastly, I also became familiar with GitLab and how it differs from GitHub, primarily through its integrated CI feature.

## What's in your way?
I think the biggest challenge is for me to become aquainted with the tools and frameworks for this class that I haven't used before. As an example, I've built multiple webapps using Angular but have never used React before. I'm also not as comfortable with python as I'd like to be for this class so will definitely need to spend time learning the syntax better. Another thing that will probably be in my way is making the transition back to using JavaScript after having programmed primarily in TypeScript all summer.

## What will you do next week?
After enjoying a three day weekend, I'll be learning more about assertions and unit testing in class. I'm quite familiar with test coverage, which is typically broken into different types such as line, function, or branch coverage; so I'm curious what metrics the class will use. In previous classes I had a habit of neglecting textbook readings, but I would really like to change that this semeter and am planning on diving into *Extreme Programming* this week.

## What are your expectations of the class?
My expectations are that even though the class may be a bit of work, I know that I will learn a lot from it. Compared to other CS courses that teach you how to program or write clean code, I think that this one will do the best job at preparing me for the real world. I hope that a lot of the knowledge I learn in this class I'll be able to take with me after I graduate.

## What's your pick-of-the-week or tip-of-the-week?
My pick of the week would have to be Kubernetes. Kubernetes is a system for managing and automatically scaling containers. I worked with it a lot this summer and it was amazing being able to deploy an application then not have to worry about the load as it was taken care of automatically.