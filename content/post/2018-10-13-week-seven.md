---
title: Week Seven
date: 2018-10-13
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 


## What did you do this past week?
Last week we finished up iterables and talked about default variables and variable passing for functions. I thought it was interesting how instead of having method overloading, python has a way of using unpacking to collect extra variables if given. I also thought it was interesting how none could be used to generate a tuple in a fuction as a default only the first time it's called rather than creating a new one each time leading to weird behaviors.

## What's in your way?
The biggest thing in my way is that I have a lot of tests, projects, and a couple of interviews next week. I have a lot of studying I need to do and will make sure that I'm more comfortable with python as a language for this class since I haven't really used it outside of the class exercises and a tiny bit on project 3. Since my other group members likely have exams for other classes it'll be challenging finding a time that most of us can meet up.

## What will you do next week?
Next week we have our first test. Aside from class my group will continue working on project 3. Specifically, I'll be starting writing the SQL scripts to gather data from our database upon an API Call and think I remember hearing that we'll be starting to learn about SQL in class. Outside of this class, I'll also have midterms for my other 3 classes as well as two phone interviews so I'll need to manage my time well.

## What was your experience of learning the basics of Python?
At first I was concerned that I would have to learn python on my own for this course, but I'm glad we went through many implementations and their explanations in class. As an upper division course, I think it was great that we were able to skip over the basics and really focus on the syntax and interfaces.

## What's your pick-of-the-week or tip-of-the-week?
My tip of the week would be to remember to get a good night's rest the night before exams. With a lot going on right now I know that sleep is something that can often be overlooked, but getting a good night's rest can help you perform better on exams and interviews.




