---
title: The Ethical Software Engineer
date: 2018-10-24
---

<img src="https://i.imgur.com/5A7zfhm.jpg" height="300" width="300" style="margin-left: unset"> 


## The Suggestion Box
If put into the situation of the first problem, I would most likely refuse to examine the computer's usage records. The primary reason is that I think a company should always stand by its word. If the CEO promises anonymity then it should be upheld; otherwise, the company might lose trust in its leadership. If we looked through the records then either confronted or fired the offender, there's a risk that other employees could find out that we examined the records. Another reason to respect employee privacy is that the employee who left the feedback may not actually attempt to sabotage the company. It's possible that the culprit just left negative feedback to test if management would stay true to their word. An additional reason is that, with a company of only 200 employees, it should be feasible to find the culprit without examining the feedback records. Since the feedback mentioned parts produced, we can at the very least narrow down the possible suspects to only those that product parts. From there, we can have managers perform detailed quality checks on the parts and make a list of workers who produce on average 1 defective part out of every 10. If within the company multiple workers work on the same part such as in an assembly line, we can simply perform the quality checks before the parts get passed to the next worker. Even though the approach mentioned above may be less efficient and use up valuable time for managers, I think it's worth using as an alternative to the risk of losing the trust in other employees. If we lost our employees' trust, they may be less inclined to provide negative feedback in the future which can make it hard to improve the workplace or act upon issues.

While I think that upholding employee anonymity should be a priority, there may be cases in which examining the computer's usage records may be the best option. If the company produces vaccines, medicine, anesthetics, or other items which could be dangerous when defective then we should break the CEO's promise of anonymity. It's hard to define the line of ethics, but I'd have to say that if a defective product could harm others then we should prioritize the quality of our products over employee trust and satisfaction. Another situation in which I would agree to view the computer's records would be if the cost of the defect is to a certain degree. If the materials we're working with are very expensive then the potential cost of defective items produced until we find the culprit might not be worth waiting. If the company is fairly new or has a tight budget and the cost of defects is over a certain threshold then the sabotage thread should not be taken lightly.

Overall, unless there are health or monetary concerns regarding defective products, then I stand by my statement earlier in that we should respect our employee's anonymity and try to find the employee without looking at the computer's records.


